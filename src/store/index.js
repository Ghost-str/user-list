import Vuex from 'vuex';
import Vue from 'vue';
import Axios from 'axios';

Vue.use(Vuex);

const index = new Vuex.Store({
    state: {
        users: []
    },
    mutations: {
        updateUsers(state) {
            Axios.get('http://jsonplaceholder.typicode.com/users')
                .then(function (response) {
                    state.users = response.data;
                })
        },
    }
});
export default index;