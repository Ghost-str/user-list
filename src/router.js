import Vue from 'vue'
import Router from 'vue-router'
import UserList from './views/UserList'
import UserInfo from './views/UserInfo';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'user-list',
            component: UserList
        },
        {
            path: '/user/:id',
            name: 'user-info',
            component: UserInfo
        },
        // {
        //   path: '/about',
        //   name: 'about',
        //   // route level code-splitting
        //   // this generates a separate chunk (about.[hash].js) for this route
        //   // which is lazy-loaded when the route is visited.
        //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        // }
    ]
})
